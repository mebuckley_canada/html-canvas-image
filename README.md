html-canvas-image
===================

Renders a png image from a supplied html container element

# Installation
bower install git@bitbucket.org:mebuckley_canada/html-canvas-image.git --saveImage


## Example component that uses html-canvas-image
```
<link rel="import" href="../../bower_components/polymer/polymer.html">
<link rel="import" href="../../bower_components/html-canvas-image/html-canvas-image.html">

<dom-module id="my-component">
  <template>

    <div id="meme-container">
    	<img src="../../images/cat-meme.png" />
    </div>
    <html-canvas-image id="memeImageGenerator" container="meme-container" filename="meme.png"></html-canvas-image>
    <hr />
    <paper-button raised on-tap="saveImage">Save Image</paper-button>
  </template>

  <script>
    (function() {
      'use strict';

      Polymer({
        is: 'my-component',

        /**
         * Generate image
         */
        saveImage: function() {
          this.$.memeImageGenerator.generateImage();
        },

        ready: function() {

        }
      });
    })();
  </script>
</dom-module>
```